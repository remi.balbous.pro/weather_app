package com.weatherapp.network

import com.weatherapp.network.models.Weather
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject


interface WeatherService {
    @GET("weather")
    fun getCurrentWeather(
        @Query("q") city: String?,
        @Query("appid") apiKey: String?
    ): Single<Weather>
}