package com.weatherapp.network.domain.usecase

import com.weatherapp.BuildConfig
import com.weatherapp.network.WeatherService
import com.weatherapp.network.models.Weather
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetCurrentWeatherRequestUseCase @Inject constructor() {

    @Inject
    lateinit var service: WeatherService

    fun execute(city: String): Single<Weather> {
        return service.getCurrentWeather(city, BuildConfig.key)
    }


}