package com.weatherapp.network.models

import com.google.gson.annotations.SerializedName

data class CityWeather(
    @SerializedName("main")
    val sky: String
)

data class CityTemperature(
    @SerializedName("temp")
    val temp: Float
)

data class Weather(
    @SerializedName("main")
    val cityTemperature: CityTemperature,
    @SerializedName("weather")
    val cityWeather: List<CityWeather>,
    @SerializedName("name")
    val cityName: String
)