package com.weatherapp.features.first

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.weatherapp.base.fragment.BaseFragment
import com.weatherapp.databinding.FragmentFirstBinding
import toothpick.config.Module
import javax.inject.Inject

class FirstFragment @Inject constructor() : BaseFragment<FragmentFirstBinding>() {

    @Inject
    lateinit var navigation: FirstNavigation

    override var injectionModule: Module = Module()

    override fun bindingView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): FragmentFirstBinding
    = FragmentFirstBinding.inflate(layoutInflater, container, false)

    override fun setUpView() {
        with(binding) {
            pushBtn.setOnClickListener {
                navigation.navigateToSecondFragment()
            }
        }
    }
}