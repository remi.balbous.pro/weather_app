package com.weatherapp.features.second

import toothpick.config.Module
import toothpick.ktp.binding.bind

class SecondFragmentModule(): Module() {
    init {
        bind<SecondContract.ViewModel>().toInstance(SecondViewModel())
    }
}