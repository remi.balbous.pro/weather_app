package com.weatherapp.features.first

import androidx.navigation.NavController
import javax.inject.Inject


class FirstNavigation() {

    @Inject
    lateinit var controller: NavController

    fun navigateToSecondFragment() {
        controller.navigate(FirstFragmentDirections.actionFirstFragmentToSecondFragment())
    }
}