package com.weatherapp.features.second

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.weatherapp.features.second.domain.usecase.ClockUseCase
import com.weatherapp.network.domain.usecase.GetCurrentWeatherRequestUseCase
import com.weatherapp.network.models.Weather
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import toothpick.ktp.KTP
import javax.inject.Inject

class SecondViewModel : SecondContract.ViewModel() {


    private val state = MutableLiveData<SecondViewState>()

    private var passed = false
    private val cityList = listOf("Rennes", "Paris", "Nantes", "Bordeaux", "Lyon")
    private var cityIndex = 0
    private var weatherList: MutableList<Weather> = mutableListOf()
    private lateinit var clock: Disposable

    @Inject
    lateinit var getCurrentWeatherRequestUseCase: GetCurrentWeatherRequestUseCase

    @Inject
    lateinit var clockUseCase: ClockUseCase

    override var viewState: LiveData<SecondViewState> = state
    override var updateIndicatorEvent: MutableLiveData<Long> = MutableLiveData<Long>()
    override var changeWaitingTextEvent: MutableLiveData<Unit> = MutableLiveData<Unit>()
    override var errorOccurredEvent: MutableLiveData<Unit> = MutableLiveData<Unit>()

    init {
        KTP
            .openRootScope()
            .openSubScope(this)
            .installModules(SecondViewModelModule())
            .inject(this)
    }

    override fun startAction() {
        if (!passed) {
            passed = true
            clock = clockUseCase.execute()
                .subscribeOn(Schedulers.io())
                .doOnNext {
                    val second = it.toInt()
                    if (cityIndex <= 4 && second % 10 == 0) {
                        getWeatherAt(cityList[cityIndex])
                        cityIndex++
                    }
                    updateIndicatorEvent.postValue(it)
                    if (second % 6 == 0 && second != 0)
                        changeWaitingTextEvent.postValue(Unit)
                    if (second >= 60) {
                        clock.dispose()
                        state.postValue(SecondViewState(weatherList.toList()))
                    }
                }
                .subscribe()
        }
    }

    override fun retry() {
        passed = false
        cityIndex = 0
        weatherList.clear()
        startAction()
    }

    private fun getWeatherAt(city: String) {
        getCurrentWeatherRequestUseCase
            .execute(city)
            .subscribeOn(Schedulers.io())
            .doOnSuccess {
                weatherList.add(it)
            }
            .doOnError {
                errorOccurredEvent.postValue(Unit)
                clock.dispose()
            }
            .subscribe()
    }
}