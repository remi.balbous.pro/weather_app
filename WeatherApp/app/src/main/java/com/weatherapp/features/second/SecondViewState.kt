package com.weatherapp.features.second

import com.weatherapp.network.models.Weather

data class SecondViewState(val weatherList: List<Weather> = listOf())
