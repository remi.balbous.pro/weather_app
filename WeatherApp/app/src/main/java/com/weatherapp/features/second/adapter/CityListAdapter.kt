package com.weatherapp.features.second.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.weatherapp.R
import com.weatherapp.databinding.WeatherItemBinding
import com.weatherapp.network.models.Weather

class CityListAdapter constructor(private val context: Activity, private val weatherList: List<Weather>)
    : ArrayAdapter<Weather>(context, R.layout.weather_item, weatherList) {

    @SuppressLint("ViewHolder", "SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = WeatherItemBinding.inflate(context.layoutInflater, parent, false)

        val degree = (weatherList[position].cityTemperature.temp - 273.15f)
        with(view) {
            tvCity.text = weatherList[position].cityName
            tvTemp.text = "%.2f".format(degree) + "°C"
            tvWeather.text = weatherList[position].cityWeather[0].sky
        }


        return view.root
    }
}