package com.weatherapp.features.second

import com.weatherapp.network.WeatherClient
import com.weatherapp.network.WeatherService
import toothpick.config.Module
import toothpick.ktp.binding.bind

class SecondViewModelModule(): Module() {

    init {
        bind<WeatherService>()
            .toInstance(WeatherClient.retrofitInstance.create(WeatherService::class.java))
    }
}