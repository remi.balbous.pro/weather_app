package com.weatherapp.features.second

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

interface SecondContract {

    open class ViewModel: androidx.lifecycle.ViewModel() {

        open lateinit var viewState: LiveData<SecondViewState>
        open lateinit var updateIndicatorEvent: MutableLiveData<Long>
        open lateinit var changeWaitingTextEvent : MutableLiveData<Unit>
        open lateinit var errorOccurredEvent: MutableLiveData<Unit>

        open fun startAction() {}
        open fun retry() {}
    }
}