package com.weatherapp.features.second.domain.usecase

import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ClockUseCase @Inject constructor() {
    fun execute(): Observable<Long> = Observable.interval(1000L, TimeUnit.MILLISECONDS)
}