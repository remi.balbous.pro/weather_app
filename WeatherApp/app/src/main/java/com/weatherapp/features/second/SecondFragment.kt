package com.weatherapp.features.second

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.weatherapp.R
import com.weatherapp.base.fragment.BaseFragment
import com.weatherapp.databinding.FragmentSecondBinding
import com.weatherapp.features.second.adapter.CityListAdapter
import com.weatherapp.network.models.Weather
import toothpick.config.Module
import javax.inject.Inject

class SecondFragment @Inject constructor() : BaseFragment<FragmentSecondBinding>() {

    private var waitingTextIndex = 0
    private var weatherList = mutableListOf<Weather>()


    private lateinit var waitingTextList: List<String>

    private lateinit var adapter: CityListAdapter

    @Inject
    lateinit var viewModel: SecondContract.ViewModel

    override var injectionModule: Module = SecondFragmentModule()

    override fun bindingView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentSecondBinding = FragmentSecondBinding.inflate(layoutInflater, container, false)

    override fun observeViewModel() {
        viewModel.viewState.observe(viewLifecycleOwner, ::renderState)

        viewModel.updateIndicatorEvent.observe(viewLifecycleOwner) {
            with(binding) {
                val indicator = (it.toDouble() / 60 * 100).toInt()
                if (indicator <= 100) {
                    progressIndicator.progress = indicator
                }
            }
        }

        viewModel.changeWaitingTextEvent.observe(viewLifecycleOwner) {
            waitingTextIndex += 1
            if (waitingTextIndex > 2)
                waitingTextIndex = 0
            binding.tvWaitingMsg.text = waitingTextList[waitingTextIndex]
        }

        viewModel.errorOccurredEvent.observe(viewLifecycleOwner) {
            Toast.makeText(context, "an error occurred !", Toast.LENGTH_SHORT).show()
            binding.tvWaitingMsg.visibility = View.GONE
            binding.progressIndicator.visibility = View.GONE
            binding.retryBtn.visibility = View.VISIBLE
        }
    }

    override fun setUpView() {

        adapter = CityListAdapter(requireActivity(), weatherList)

        waitingTextList = listOf(
            getString(R.string.second_frag_waiting_msg_1),
            getString(R.string.second_frag_waiting_msg_2),
            getString(R.string.second_frag_waiting_msg_3)
        )

        with(binding) {
            lvItems.adapter = adapter
            tvWaitingMsg.text = waitingTextList[waitingTextIndex]
            retryBtn.setOnClickListener {
                waitingTextIndex = 0
                progressIndicator.progress = 0

                tvWaitingMsg.visibility = View.VISIBLE
                progressIndicator.visibility = View.VISIBLE
                it.visibility = View.GONE

                weatherList.clear()
                adapter.notifyDataSetChanged()
                binding.lvItems.adapter = adapter

                viewModel.retry()
            }
        }

        viewModel.startAction()
    }

    private fun renderState(state: SecondViewState) {
        weatherList.clear()
        weatherList.addAll(state.weatherList)
        adapter.notifyDataSetChanged()
        binding.lvItems.adapter = adapter

        if (state.weatherList.isNotEmpty()) {
            binding.tvWaitingMsg.visibility = View.GONE
            binding.progressIndicator.visibility = View.GONE
            binding.retryBtn.visibility = View.VISIBLE
        }
    }
}