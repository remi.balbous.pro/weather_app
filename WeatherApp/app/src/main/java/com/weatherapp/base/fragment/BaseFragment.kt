package com.weatherapp.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import toothpick.config.Module
import toothpick.ktp.KTP

abstract class BaseFragment<B : ViewBinding> : Fragment() {

    protected lateinit var binding: B

    protected abstract var injectionModule: Module

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = bindingView(layoutInflater, container, savedInstanceState)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }
    override fun onStart() {
        super.onStart()
        setUpView()
    }

    protected abstract fun bindingView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): B

    open fun setUpView() {}

    open fun observeViewModel() {}

    private fun injectDependencies() {
        KTP
            .openRootScope()
            .openSubScope(this)
            .installModules(BaseFragmentModule(this), injectionModule)
            .inject(this)
    }
}