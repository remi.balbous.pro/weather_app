package com.weatherapp.base.fragment

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import toothpick.config.Module
import toothpick.ktp.binding.bind

class BaseFragmentModule(fragment: Fragment): Module() {
    init {
        bind<Fragment>().toInstance(fragment)
        bind<NavController>().toInstance(fragment.findNavController())
    }
}