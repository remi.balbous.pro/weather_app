package com.weatherapp.common

import android.app.Activity
import com.weatherapp.network.WeatherClient
import com.weatherapp.network.WeatherClient.retrofitInstance
import com.weatherapp.network.WeatherService
import toothpick.config.Module
import toothpick.ktp.binding.bind

class ActivityModule(activity: MainActivity): Module() {
    init {
        bind<Activity>().toInstance(activity)
    }
}