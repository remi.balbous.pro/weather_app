package com.weatherapp.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.weatherapp.databinding.ActivityMainBinding
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import toothpick.ktp.KTP

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        RxJavaPlugins.setErrorHandler { Log.e("API_ERROR", it.toString()) }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun injectDependencies() {
        KTP
            .openRootScope()
            .installModules(ActivityModule(this))
            .inject(this)
    }
}